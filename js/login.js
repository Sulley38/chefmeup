// A ejecutar cuando se cargue la página
$(function() {
	
	// Textos por defecto de los campos de los formularios
	defaultTextLogin = {'username' : 'Nombre de usuario', 
			'password' : 'Contraseña',
			'password2' : 'Repetir contraseña',
			'email' : 'Correo electrónico',
			'email2' : 'Repetir correo electrónico',
			'city' : 'Localidad'};
	
	// Al tomar el foco un campo de texto
	$('#page_login form input:text').focus(function() {
		// Si el texto actual es el texto por defecto, quitarlo
		if ($(this).val() == defaultTextLogin[$(this).attr('name')]) {
			$(this).val('');
			$(this).css('font-style', 'normal');
			// Si además es un input de contraseña, convertirlo en campo de contraseña
			if ($(this).attr('name').substring(0, 8) == 'password')
				$(this).prop('type', 'password');
		}
	});
	
	// Al perder el foco un campo de texto
	$('#page_login form input:text').blur(function() {
		// Si el texto introducido es vacío, volver a poner el texto por defecto
		if ($(this).val() == '') {
			$(this).val( defaultTextLogin[$(this).attr('name')] );
			$(this).css('font-style', 'italic');
			// Si además es un input de contraseña, convertirlo en campo de texto
			if ($(this).attr('name').substring(0, 8) == 'password')
				$(this).prop('type', 'text');
		}
	});
	
	// Al cortar/copiar/pegar en los campos de validación
	$('#page_login form input[name$="2"]').bind('cut copy paste', function(e) {
		// Impedir la acción
		e.preventDefault();
	});
	
	// Al enviar el formulario de iniciar sesión
	$('#form_login').submit(function() {
		// Mostrar la barra de carga y ocultar el texto de error
		$('#form_login p.message').hide();
		$('#form_login p.loading').show();
		// Enviar datos del formulario
		$.post('pages/login.php', $('#form_login').serialize(), function(result) {
			if (result == 'OK') {
				// Acceso correcto
				window.location.href = 'index.php';
			} else {
				// Acceso incorrecto: ocultar la barra de carga y mostrar el texto de error
				$('#form_login p.loading').hide();
				$('#form_login p.message').css('color', '#FF0000').html('Datos de acceso incorrectos').show();
			}
		});
		// Evitar que se envíe el formulario por el mecanismo habitual
		return false;
	});
	
	// Al enviar el formulario de contraseña olvidada
	$('#form_recover').submit(function() {
		// Mostrar la barra de carga y ocultar el texto de éxito/error
		$('#form_recover p.message').hide();
		$('#form_recover p.loading').show();
		// Enviar datos del formulario
		$.post('pages/login.php', $('#form_recover').serialize(), function(result) {
			if (result == 'OK') {
				// Reseteo correcto
				$('#form_recover p.loading').hide();
				$('#form_recover p.message').css('color', '#02A000').html('Se le ha enviado un e-mail con la nueva contraseña').show();
			} else if (result == 'NOT_FOUND') {
				// E-mail incorrecto
				$('#form_recover p.loading').hide();
				$('#form_recover p.message').css('color', '#FF0000').html('Dirección de correo incorrecta').show();
			} else {
				// Problema del servidor
				$('#form_recover p.loading').hide();
				$('#form_recover p.message').css('color', '#FF0000').html('Ocurrió un error inesperado').show();
			}
		});
		// Evitar que se envíe el formulario por el mecanismo habitual
		return false;
	});
	
	// Al enviar el formulario de crear cuenta
	$('#form_register').submit(function() {
		// Mostrar la barra de carga y ocultar los textos de éxito/error
		$('#form_register p.message').hide();
		$('#form_register p.loading').show();
		// Validar el formulario
		if (validarFormulario()) {
			// Enviar datos del formulario
			$.post('pages/login.php', $('#form_register').serialize(), function(result) {
				if (result == 'OK') {
					// Registro correcto
					$('#form_register p.loading').hide();
					$('#form_register p.message').css('color', '#02A000').html('Usuario registrado con éxito').show();
				} else if (result == 'USER_EXISTS') {
					// Nombre de usuario existente
					$('#form_register p.loading').hide();
					$('#form_register p.message').css('color', '#FF0000').html('Ya existe una cuenta con ese nombre de usuario').show();
				} else if (result == 'EMAIL_EXISTS') {
					// Correo electrónico ya registrado
					$('#form_register p.loading').hide();
					$('#form_register p.message').css('color', '#FF0000').html('Ya existe una cuenta asociada a ese correo electrónico').show();
				} else {
					// Datos inválidos
					$('#form_register p.loading').hide();
					$('#form_register p.message').css('color', '#FF0000').html('Datos inválidos').show();
				}
			});
		} else {
			// Mensaje de error personalizado según validarFormulario()
			$('#form_register p.loading').hide();
			$('#form_register p.message').css('color', '#FF0000').show();
		}
		// Evitar que se envíe el formulario por el mecanismo habitual
		return false;
	});
	
});

function validarFormulario() {
	var form = document.forms['form_register'];
	// Nombre de usuario
	if (form.username.value == null || form.username.value == '' || form.username.value == defaultTextLogin['username']) {
		$('#form_register p.message').html('Introduzca un nombre de usuario');
		return false;
	}
	// Longitud del nombre de usuario
	if (form.username.value.length < 3) {
		$('#form_register p.message').html('El nombre de usuario debe contener al menos 3 caracteres');
		return false;
	}
	// Contraseña
	if (form.password.value == null || form.password.value == '' || form.password.value == defaultTextLogin['password']
	 || form.password2.value == null || form.password2.value == '' || form.password2.value == defaultTextLogin['password2']) {
		$('#form_register p.message').html('Introduzca una contraseña');
		return false;
	}
	// Las dos contraseñas coinciden
	if (form.password.value != form.password2.value) {
		$('#form_register p.message').html('Las contraseñas no coinciden');
		return false;
	}
	// La contraseña es válida
	if (form.password.value.length < 6) {
		$('#form_register p.message').html('La contraseña debe contener al menos 6 caracteres');
		return false;
	}
	// Correo electrónico
	if (form.email.value == null || form.email.value == '' || form.email.value == defaultTextLogin['email']
	 || form.email2.value == null || form.email2.value == '' || form.email2.value == defaultTextLogin['email2']) {
		$('#form_register p.message').html('Introduzca un correo electrónico');
		return false;
	}
	// Los dos correos coinciden
	if (form.email.value != form.email2.value) {
		$('#form_register p.message').html('Las direcciones de correo no coinciden');
		return false;
	}
	// El correo es válido
	var reg = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
	if (!reg.test(form.email.value)) {
		$('#form_register p.message').html('La dirección de correo electrónico no es válida');
		return false;
	}
	// Localidad
	if (form.city.value == null || form.city.value == '' || form.city.value == defaultTextLogin['city']) {
		$('#form_register p.message').html('Introduzca una localidad');
		return false;
	}
	return true;
}