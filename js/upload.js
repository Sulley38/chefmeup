// A ejecutar cuando se cargue la página
$(function() {
	
	var dificultyLevels = ["Muy fácil", "Fácil", "Intermedio", "Difícil", "Muy difícil"];

	// Crear el slider de dificultad
	$('#form_recipe span#dificultad').slider({animate: 'fast', min: 1, max: 5, range: 'min',
		 slide: function(event, ui) {
			 // Actualizar el texto de dificultad
			 $('#form_recipe span#dificultadText').html(dificultyLevels[ui.value-1]);
		 }
	});
	
	// Crear los objetos spinner para seleccionar nº de personas y tiempo
	$('#form_recipe input[name="personas"]').spinner({min: 1, max: 99}).attr('readOnly', 'true');
	$('#form_recipe input[name="horas"]').spinner({min: 0, max: 9}).attr('readOnly', 'true');
	$('#form_recipe input[name="minutos"]').spinner({step: 5, spin: function( event, ui ) {
    			// Cambiar la hora segun cambian los minutos
    			horas = parseInt($('#form_recipe input[name="horas"]').val());
    			// Añadir uno a la hora
    			if ( ui.value > 55 ) {
    				$( this ).spinner( "value", 0 );    				
    				$('#form_recipe input[name="horas"]').val(horas + 1);
    				return false;
    			}
    			// Reducir uno la hora
    			if ( ui.value < 0 && horas > 0) {
    				$( this ).spinner( "value", 55 );    				
    				$('#form_recipe input[name="horas"]').val(horas - 1);
    				return false;
    			}
    			// Impedir que se baje de los 5 minutos
    			if ( ui.value < 5 && horas == 0) {
    				return false;
    			}
          }}).attr('readOnly', 'true');
	
	// Al hacer click en el botón de añadir ingrediente
	$('#form_recipe input#addIngredient').click(function() {
		// Añadir nuevo ingrediente oculto y con identificador único
		$('#form_recipe div#ingredientes').append('<p style="display: none;">Ingrediente: <input type="text" name="ingrediente[]" /><input type="button" /></p>');
		// Mostrar el ingrediente añadido con una animación
		$('#form_recipe div#ingredientes p:last-child').show("slow");
	});
	
	// Al hacer click en el botón de añadir paso
	$('#form_recipe input#addStep').click(function() {
		// Añadir nuevo paso oculto y con identificador único
		$('#form_recipe div#pasos').append('<p style="display: none;">Paso: <input type="text" name="paso[]" /><input type="button" /></p>');
		// Mostrar el paso añadido con una animación
		$('#form_recipe div#pasos p:last-child').show("slow");
	});
	
	// Al hacer click en un botón de eliminar ingrediente o paso
	$('#form_recipe div#ingredientes, #form_recipe div#pasos').on("click", "input:button", function() {
		// Ocultar la línea con una animación
		$(this).parent().hide("slow", function() {
			// Eliminarla
			$(this).remove();
		});
	});

	// Al enviar el formulario de subir receta
	$('#form_recipe').submit(function() {
		// Mostrar la barra de carga y ocultar el texto de éxito/error
		$('#form_recipe p.message').hide();
		$('#form_recipe p.loading').show();
		// Validar el formulario
		if (validarCampos()) {
			// Añadir la información de la dificultad a los datos
			datos = $('#form_recipe').serialize();
			datos = datos + "&dificultad=" + $('#form_recipe span#dificultad').slider('value');
			// Hacer una peticion de AJAX con los datos del formulario
			$.post('pages/upload.php', datos, function(result) {
				if (result == true) {
					// Mensaje de éxito
					$('#form_recipe p.loading').hide();
					$('#form_recipe p.message').css('color', '#02A000').html('Receta añadida. En unos instantes se recargará la página').show();
					// Recargar la página un instante después
					window.setTimeout(function() {location.reload();}, 2000);
				} else {
					// Mensaje de error
					$('#form_recipe p.loading').hide();
					$('#form_recipe p.message').css('color', '#FF0000').html('Error inesperado').show();
				}
			});
		} else {
			// Mensaje de error: campos vacíos
			$('#form_recipe p.loading').hide();
			$('#form_recipe p.message').css('color', '#FF0000').html('No puede haber campos en blanco').show();
		}
		// Evitar que se envíe el formulario por el mecanismo habitual
		return false;
	});

});

// Comprueba si hay algún campo en blanco en el formulario
function validarCampos() {
	var valido = true;
	// Comprobar que ningún campo sea vacio
	$('#form_recipe input:text').each(function() {
		if ($(this).val() == null || $(this).val() == '')
			valido = false;
	});
	return valido;
}