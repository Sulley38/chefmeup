// A ejecutar cuando se cargue la página
$(function() {
	// Variable que contiene la página que se está mostrando
	currentPage = '#page_start'; // Página inicial
	
	// Al hacer click en un botón del menú de navegación
	$('#nav-start').click(function(e) { mostrarPagina('#page_start', e); });
	$('#nav-list').click(function(e) { mostrarPagina('#page_listrecipes', e); });
	$('#nav-upload').click(function(e) { mostrarPagina('#page_upload', e); });
	$('#nav-myrecipes').click(function(e) { mostrarPagina('#page_myrecipes', e); });
	$('#nav-login').click(function(e) { mostrarPagina('#page_login', e); });
	$('#nav-logout').click(function(e) { window.location.href = 'pages/logout.php'; });
	
	// Al hacer click en una nota con enlace a otra página
	$('#page div.note a').click(function(e) { mostrarPagina($(this).attr('href'), e); });

});

// Cambia la página que se está mostrando
function mostrarPagina(pagina, evento) {
	// Impedir la acción por defecto del navegador
	evento.preventDefault();
	// Cambiar la página que se muestra (propiedades display)
	if (currentPage != pagina) {
		$('#page').slideUp(500, function() {
			$(currentPage).css('display', 'none');
			currentPage = pagina;
			$(currentPage).css('display', 'block');
			$('#page').slideDown(500);
		});
	}
}