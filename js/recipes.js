// A ejecutar cuando se cargue la página
$(function() {

	// Al enviar el formulario de ver recetas
	$('#form_listrecipes').submit(function() {
		// Solicitar la lista de recetas e introducirla en el bloque de resultados
		$('#page_listrecipes div#recipelist').load('pages/listrecipes.php', $("#form_listrecipes").serializeArray());
		// Evitar que se envíe el formulario por el mecanismo habitual
		return false;
	});
	
	// Al hacer click en un elemento de la paginación
	$(document).on('click', '#page_listrecipes p.list-pages a', function(e) {
		// Impedir la acción por defecto del navegador
		e.preventDefault();
		// Calcular qué página estamos viendo y cuáles han sido los parámetros de listado
		var current = $('#page_listrecipes p.list-pages span').attr('id').substr(5);
		var params = $('#page_listrecipes div#recipelist p#list-params').attr('class').split(' ');
		// Comprobar qué boton hemos pulsado
		if ($(this).attr('id') == 'previous-page') {
			// Botón "Anterior": pedir por AJAX la página anterior
			$('#page_listrecipes div#recipelist').load('pages/listrecipes.php', {rule : params[0], order : params[1], results : params[2], page : parseInt(current) - 1 });
		} else if ($(this).attr('id') == 'next-page') {
			// Botón "Siguiente": pedir por AJAX la página siguiente
			$('#page_listrecipes div#recipelist').load('pages/listrecipes.php', {rule : params[0], order : params[1], results : params[2], page : parseInt(current) + 1 });
		} else {
			// Botón de página concreta: pedir por AJAX esa página
			var requestpage = $(this).attr('id').substr(5);
			$('#page_listrecipes div#recipelist').load('pages/listrecipes.php', {rule : params[0], order : params[1], results : params[2], page : requestpage });
		}
	});
	
	// Al hacer click en el icono de borrar receta
	$(document).on('click', 'div.recipe a.delete-recipe', function(e) {
		// Impedir la acción por defecto del navegador
		e.preventDefault();
		// Confirmación del usuario
		if (confirm('¿Seguro que quiere eliminar la receta?\r\nEsta acción no se puede deshacer')) {
			// Solicitar la eliminación de la receta
			recipeId = $(this).parent().attr('class').split(' ')[1]; // Recipe ID
			$.post('pages/listrecipes.php', {id : recipeId, remove: true}, function(result) {
				if (result == true) {
					// Desaparece gradualmente la receta
					$('#page_myrecipes div.' + recipeId).fadeOut(800, function() {
						// Se elimina del documento entero
						$('#page_start div.' + recipeId).remove();
						$('#page_listrecipes div.' + recipeId).remove();
						$('#page_myrecipes div.' + recipeId).remove();
						// Si era la última receta del usuario, mostrar el mensaje de añadir receta
						if ($('#page_myrecipes').children().length == 0) {
							$('#page_myrecipes').append('<div class="note"><p>Parece que aún no has subido ninguna receta.<br/>¿Por qué no <a href="#page_upload">añades</a> una?</p></div>');
							// Añadirle el manejador onclick
							$('#page div.note a').click(function(e) {
								e.preventDefault();
								mostrarPagina($(this).attr('href'));
							});
						}
					});
				}
			});
		}
	});
	
	// Al hacer click en un botón de abrir/cerrar receta
	$(document).on('click', 'div.recipe a.toggle-recipe', function(e) {
		// Impedir la acción por defecto del navegador
		e.preventDefault();
		recipeId = $(this).parent().attr('class').split(' ')[1]; // Recipe ID
		open = ($(this).siblings('div.recipe-content').length == 0); // ¿Abriendo o cerrando?
		if (open) {
			// Solicitar el contenido de la receta por AJAX
			$.post('pages/listrecipes.php', {id : recipeId}, function(response) {
				// Cambiar el texto del botón
				$(currentPage + ' div.' + recipeId + ' a.toggle-recipe').html('Cerrar');
				// Añadirlo al final de la receta
				$(currentPage + ' div.' + recipeId).append(response);
				$(currentPage + ' div.' + recipeId + ' div.recipe-content').slideDown(500);
			});
		} else {
			// Cambiar el texto del botón
			$(currentPage + ' div.' + recipeId + ' a.toggle-recipe').html('Abrir');
			// Eliminar el contenido de la receta
			$(currentPage + ' div.' + recipeId + ' div.recipe-content').slideUp(500, function() {
				$(this).remove();
			});
		}
	});
	
	// Al hacer click en un botón de voto positivo
	$(document).on('click', 'div.recipe img.voteup', function(e) {
		recipeId = $(this).parent().parent().attr('class').split(' ')[1]; // Recipe ID
		// Enviar petición AJAX de voto positivo, y sustituir el párrafo por la nueva puntuación
		$(this).parent().load('pages/listrecipes.php', {id : recipeId, vote : 1});
	});
	
	// Al hacer click en un botón de voto negativo
	$(document).on('click', 'div.recipe img.votedown', function(e) {
		recipeId = $(this).parent().parent().attr('class').split(' ')[1]; // Recipe ID
		// Enviar petición AJAX de voto negativo, y sustituir el párrafo por la nueva puntuación
		$(this).parent().load('pages/listrecipes.php', {id : recipeId, vote : -1});
	});
	
});
