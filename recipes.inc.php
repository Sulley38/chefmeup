<?php

/**
 * Añade una nueva receta al fichero XML.
 * Se le pasan por parámetro todos los datos de la receta.
 * Todos son strings excepto ingredientes y pasos, que son arrays de strings.
 */
function addRecipe($titulo, $autor, $dificultad, $tiempo, $personas, $ingredientes, $pasos) {
	// Abrir fichero XML
	$recetas = simplexml_load_file(dirname(__FILE__) . '/recetas.xml');
	// Añadir nuevos nodos
	$nuevaReceta = $recetas->addChild('receta'); // Receta
	$recetas['ultId'] = $recetas['ultId'] + 1; // Actualizar valor ultId
	$nuevaReceta->addAttribute('id', 'recipe' . $recetas['ultId']); // Atributo id
	$nuevaReceta->addChild('titulo', $titulo); // Título
	$nuevaReceta->addChild('autor', $autor); // Autor
	$nuevaReceta->addChild('fecha', time()); // Fecha de hoy (segundos desde Epoch)
	$nuevaReceta->addChild('puntuacion', 0); // Puntuación (inicialmente 0)
	$nuevaReceta->addChild('dificultad', $dificultad); // Dificultad
	$nuevaReceta->addChild('tiempo', $tiempo); // Tiempo necesario (en minutos)
	$nuevaReceta->addChild('personas', $personas); // Comensales
	// Ingredientes
	$nodoIngredientes = $nuevaReceta->addChild('ingredientes');
	foreach ($ingredientes as $ingrediente)
		$nodoIngredientes->addChild('ingrediente', $ingrediente);
	// Pasos
	$nodoPasos = $nuevaReceta->addChild('pasos');
	foreach ($pasos as $paso)
		$nodoPasos->addChild('paso', $paso);
	// Guardar los cambios
	$dom = new DOMDocument('1.0');
	$dom->preserveWhiteSpace = false;
	$dom->formatOutput = true;
	$dom->loadXML($recetas->asXML());
	$dom->save(dirname(__FILE__) . '/recetas.xml');
	return true;
}

/**
 * Devuelve true si $id es un identificador de receta que cumple con el formato esperado,
 * del tipo 'recipeX' donde X es un entero.
 */
function isValidRecipeId($id) {
	return (preg_match("/^recipe[0-9]+$/", $id) === 1);
}

/**
 * Convierte una cantidad de minutos en un string del tipo "X hora(s) y Y minutos".
 */
function timeToString($minutes) {
	$hours = floor($minutes / 60);
	$minutes = $minutes % 60;
	$timeString = '';
	if ($hours > 0) {
		$timeString .= $hours . ' hora';
		if ($hours > 1) $timeString .= 's';
	}
	if ($minutes > 0) {
		if ($hours > 0) $timeString .= ' y ';
		$timeString .= $minutes . ' minutos';
	}
	return $timeString;
}

/**
 * Devuelve la cabecera de una receta cuyo identificador se pasa por parámetro.
 * La cabecera es un bloque que contiene su título, autor, dificultad, tiempo y número de comensales.
 * El parámetro delete indica si hay que incluir el icono de eliminar receta (por defecto, no).
 * Si no se incluye el de borrado, se incluyen los de votación.
 */
function getRecipeHeader($id, $delete = false) {
	// Textos de la dificultad
	$dificultyLevels = array("Muy fácil", "Fácil", "Intermedio", "Difícil", "Muy difícil");
	// Abrir fichero XML
	$recetas = simplexml_load_file(dirname(__FILE__) . '/recetas.xml');
	// Buscar la receta solicitada
	$receta = $recetas->xpath('/recetas/receta[@id="'. $id .'"]');
	$receta = $receta[0];
	// Construir el contenido de la receta
	$html = '<div class="recipe ' . $id . '">' . "\r\n";
	$html .= "<!-- Receta : $receta->titulo -->" . "\r\n";
	$html .= '<h1>' . $receta->titulo . '</h1>' . "\r\n";
	$html .= '<h2>Enviado el ' . date('j/n/Y', intval($receta->fecha)) . ' por ' . $receta->autor . '</h2>' . "\r\n";
	$html .= '<p>Dificultad: ' . $dificultyLevels[$receta->dificultad - 1] . '  &diams; ';
	$html .= 'Tiempo de preparaci&oacute;n: ' . timeToString($receta->tiempo) . '  &diams; ';
	$html .= 'Para ' . $receta->personas . ' personas</p>' . "\r\n";
	if ($delete) {
		// Incluir icono de borrado
		$html .= '<a class="delete-recipe" href="#"><img src="images/trash.png" alt="Eliminar" width="42" height="42" /></a>' . "\r\n";
	} else {
		// Incluir botones de votación
		$html .= '<p>Valoraci&oacute;n de los usuarios: '. $receta->puntuacion;
		$html .= ' <img class="voteup" src="images/voteup.gif" alt="Voto positivo" width="32" height="32" />';
		$html .= ' <img class="votedown" src="images/votedown.gif" alt="Voto negativo" width="32" height="32" /></p>' . "\r\n";
	}
	$html .= '<a class="toggle-recipe" href="#">Abrir</a>' . "\r\n";
	$html .= '</div>' . "\r\n";
	return $html;
}

/**
 * Devuelve el contenido de una receta cuyo identificador se pasa por parámetro.
 * El contenido es un bloque con los ingredientes y los pasos de la receta.
 */
function getRecipeContent($id) {
	// Abrir fichero XML
	$recetas = simplexml_load_file(dirname(__FILE__) . '/recetas.xml');
	// Buscar la receta solicitada
	$receta = $recetas->xpath('/recetas/receta[@id="'. $id .'"]');
	$receta = $receta[0];
	// Construir el contenido de la receta
	$html = '<div class="recipe-content" style="display: none;">' . "\r\n";
	$html .= '<h3>Ingredientes:</h3>' . "\r\n";
	$html .= '<ul>' . "\r\n";
	foreach ($receta->ingredientes->ingrediente as $ingrediente)
		$html .= '<li>' . $ingrediente . '</li>' . "\r\n";
	$html .= '</ul>' . "\r\n";
	$html .= '<h3>Pasos:</h3>' . "\r\n";
	$html .= '<ol>' . "\r\n";
	foreach ($receta->pasos->paso as $paso)
		$html .= '<li>' . $paso . '</li>' . "\r\n";
	$html .= '</ol>' . "\r\n";
	$html .= '</div>' . "\r\n";
	return $html;
}

/**
 * Devuelve un string con el nombre de usuario del autor de la receta cuyo identificador se pasa por parámetro
 */
function getRecipeAuthor($id) {
	// Abrir fichero XML
	$recetas = simplexml_load_file(dirname(__FILE__) . '/recetas.xml');
	// Buscar la receta solicitada
	$receta = $recetas->xpath('/recetas/receta[@id="'. $id .'"]');
	$receta = $receta[0];
	// Devolver el autor
	return $receta->autor;
}

/**
 * Funcion auxiliar: función de comparación de las recetas para ordenaciones basadas en strings,
 * Es llamada por la función usort() : función getRecipeList() en este mismo fichero.
 */
function recipeStrCmp($a, $b) {
	$valA = $a->$GLOBALS['cmpCriterio'];
	$valB = $b->$GLOBALS['cmpCriterio'];
	$order = strcasecmp($valA[0], $valB[0]);
	if ($order == 0 && $GLOBALS['cmpCriterio'] != 'titulo') {
		// En caso de empate, ordenar por título
		$valA = $a->titulo;
		$valB = $b->titulo;
		$order = strcasecmp($valA[0], $valB[0]);
	}
	return $GLOBALS['cmpOrden'] * $order;
}

/**
 * Funcion auxiliar: función de comparación de las recetas para ordenaciones basadas en enteros,
 * Es llamada por la función usort() : función getRecipeList() en este mismo fichero, y desde start.php.
 */
function recipeIntCmp($a, $b) {
	$valA = $a->$GLOBALS['cmpCriterio'];
	$valB = $b->$GLOBALS['cmpCriterio'];
	$order = intval($valA[0]) - intval($valB[0]);
	if ($order == 0) {
		// En caso de empate, ordenar por título
		$valA = $a->titulo;
		$valB = $b->titulo;
		$order = strcasecmp($valA[0], $valB[0]);
	}
	return $GLOBALS['cmpOrden'] * $order;
}

/**
 * Devuelve las recetas correspondientes a la página $pagina ordenadas según el criterio $criterio.
 * Las recetas se paginan en bloques de $elementos recetas cada uno.
 * El sentido del orden (ascendente o descendente) lo da $orden.
 */
function getRecipeList($pagina, $criterio, $orden, $elementos) {
	// Cambiar a minúsculas y quitar tildes para que $criterio coincida con las etiquetas XML
	$notAllowed = array('á','é','í','ó','ú');
	$allowed = array('a','e','i','o','u');
	$criterio = str_replace($notAllowed, $allowed, strtolower($criterio));
	
	// Definimos unas variables globales para que la funcion recipeCmp use los criterios de ordenación indicados
	$GLOBALS['cmpCriterio'] = $criterio;
	$GLOBALS['cmpOrden'] = ($orden == "Ascendente") ? 1 : -1;
	
	// Cargamos un array con todas las recetas
	$recetas = simplexml_load_file(dirname(__FILE__) . '/recetas.xml');
	$recetas = $recetas->xpath('/recetas/receta');

	// Ordenamos el array de recetas usando la función de comparación adecuada
	if ($criterio == 'titulo' || $criterio == 'autor')
		usort($recetas, 'recipeStrCmp');
	else
		usort($recetas, 'recipeIntCmp');

	// Insertar un párrafo con la paginación
	$ultPagina = ceil(count($recetas) / $elementos);
	$html = paginate($pagina, $ultPagina);
	
	// Ir concatenando las recetas que corresponden a la página solicitada
	$index = $elementos * ($pagina - 1);
	while ($index < $elementos * $pagina && $index < count($recetas)) {
		// Mientras no se alcance la última receta de la página y aún haya más recetas
		$html .= getRecipeHeader($recetas[$index]['id']);
		$index++;
	}
	
	// Insertar otro párrafo igual con la paginación
	$html .= paginate($pagina, $ultPagina);
	
	return $html;
}

/**
 * Devuelve un párrafo con la estructura de páginas del resultado.
 * Estilo: << Anterior 1 2 ... $total Siguiente >>
 */
function paginate($current, $total) {
	$html = '<p class="list-pages">';
	if ($current > 1) // Página anterior
		$html .= '<a id="previous-page" href="#"><< Anterior</a>';
	else
		$html .= '<< Anterior';
	for ($index = 1; $index < $current; $index++) // Anteriores
		$html .= ' <a id="page-' . $index . '" href="#">' . $index . '</a>';
	$html .= ' <span id="page-' . $current . '">' . $current . '</span>'; // Actual
	for ($index = $current + 1; $index <= $total; $index++) // Posteriores
		$html .= ' <a id="page-' . $index . '" href="#">' . $index . '</a>';
	if ($current < $total) // Página siguiente
		$html .= ' <a id="next-page" href="#">Siguiente >></a>';
	else
		$html .= ' Siguiente >>';
	$html .= '</p>' . "\r\n";
	return $html;
}

/**
 * Modifica la puntuación de la receta cuyo identificador se pasa por parámetro.
 * Se puede sumar ($add = true) o restar ($add = false) un punto.
 * Devuelve el texto que debería mostrarse en la página tras dar el voto.
 */
function voteRecipe($id, $add) {
	// Abrir fichero XML
	$recetas = simplexml_load_file(dirname(__FILE__) . '/recetas.xml');
	// Buscar la receta solicitada
	$receta = $recetas->xpath('/recetas/receta[@id="'. $id .'"]');
	$receta = $receta[0];
	// Modificar la puntuación acordemente
	if ($add)
		$receta->puntuacion = $receta->puntuacion + 1;
	else
		$receta->puntuacion = $receta->puntuacion - 1;
	// Guardar los cambios
	$dom = new DOMDocument('1.0');
	$dom->preserveWhiteSpace = false;
	$dom->formatOutput = true;
	$dom->loadXML($recetas->asXML());
	$dom->save(dirname(__FILE__) . '/recetas.xml');
	return '<p>Valoraci&oacute;n de los usuarios: '. $receta->puntuacion . '</p>';
}

/**
 * Elimina del fichero XML la receta cuyo identificador se pasa por parámetro.
 * Devuelve true en caso de éxito.
 */
function removeRecipe($id) {
	// Abrir fichero XML
	$recetas = simplexml_load_file(dirname(__FILE__) . '/recetas.xml');
	// Buscar la receta solicitada
	$receta = $recetas->xpath('/recetas/receta[@id="'. $id .'"]');
	$receta = $receta[0];
	// Eliminarla y guardar los cambios
	unset($receta[0]);
	$dom = new DOMDocument('1.0');
	$dom->preserveWhiteSpace = false;
	$dom->formatOutput = true;
	$dom->loadXML($recetas->asXML());
	$dom->save(dirname(__FILE__) . '/recetas.xml');
	return true;
}

?>
