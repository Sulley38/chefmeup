<?php

// No introducir el ID de sesión en los formularios
ini_set('session.use_trans_sid', '0');

// Iniciar/reanudar sesión de PHP
session_start();

// Declaración de páginas
$paginas = array('start.php', 'listrecipes.php', 'upload.php', 'myrecipes.php');

// Añadir la página de inciar sesión si el usuario no está logueado
if (!isset($_SESSION['username'])) {
	array_push($paginas, 'login.php');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>ChefMeUp!</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="description" content="Tu cat&aacute;logo de recetas on-line" />
		<meta name="keywords" content="chef, me, up, chefmeup, recetas, recipes, cocina, cooking, f&aacute;cil, easy" />
		<link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.3.custom.css" />
		<link rel="stylesheet" type="text/css" href="css/main.css" />
		<link rel="stylesheet" type="text/css" href="css/recipes.css" />
		<link rel="stylesheet" type="text/css" href="css/upload.css" />
		<link rel="stylesheet" type="text/css" href="css/login.css" />
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
		<script type="text/javascript" src="js/main.js"></script>
		<script type="text/javascript" src="js/recipes.js"></script>
		<script type="text/javascript" src="js/upload.js"></script>
		<script type="text/javascript" src="js/login.js"></script>
	</head>
	<body>
		<div id="content">
			<!-- Cabecera -->
			<div id="header">
				<img src="images/header.png" alt="ChefMeUp!" width="500" height="150" />
			</div>
			<!-- Menú de navegación -->
			<div id="nav">
				<ul>
					<li><a id="nav-start" href="#start">Inicio</a></li>
					<li><a id="nav-list" href="#list">Ver recetas</a></li>
					<li><a id="nav-upload" href="#upload">Subir una receta</a></li>
					<li><a id="nav-myrecipes" href="#myrecipes">Mis recetas</a></li>
<?php if (!isset($_SESSION['username'])) { ?>
					<li><a id="nav-login" href="#login">Iniciar sesi&oacute;n</a></li>
<?php } else { ?>
					<li><a id="nav-logout" href="#logout">Cerrar sesi&oacute;n</a></li>
<?php } ?>
				</ul>
			</div>
			<!-- Comienza bloque de páginas -->
			<div id="page">
<?php
				// Cargar cada página
				foreach ($paginas as $pagina) {
					require_once 'pages/' . $pagina;
				}
?>
			</div>
			<!-- Termina bloque de páginas -->
			<div id="bottom-frame"></div>
			<div id="push"></div> <!-- Espacio reservado para el pie de página -->
		</div>
		<!-- Pie de página -->
		<div id="footer">
			Iv&aacute;n Matellanes y Asier Mujika |
			<a href="http://validator.w3.org/check?uri=referer">
				<img src="http://www.w3.org/Icons/valid-xhtml10" alt="Valid XHTML 1.0 Strict" height="25" width="71" />
			</a> |
			<a href="http://jigsaw.w3.org/css-validator/check/referer">
				<img src="http://jigsaw.w3.org/css-validator/images/vcss" alt="¡CSS Válido!" height="25" width="71" />
			</a>
		</div>
	</body>
</html>