<?php

// Utilizar las funciones de gestión de recetas
require_once dirname(__FILE__) . '/../recipes.inc.php';

// Iniciar/reanudar la sesión de PHP (si no se ha hecho ya)
if (session_id() === '') {
	session_start();
}

// Comprobar si se dispone de toda la información necesaria para añadir una receta
if (isset($_SESSION['username']) && isset($_POST['titulo']) && isset($_POST['dificultad']) && isset($_POST['horas'])
	&& isset($_POST['minutos']) && isset($_POST['personas']) && isset($_POST['ingrediente']) && isset($_POST['paso'])) {
	// Sanear los datos de entrada, por si las moscas...
	$tituloReceta = filter_input(INPUT_POST, 'titulo', FILTER_SANITIZE_STRING);
	$autorReceta = $_SESSION['username'];
	$dificultadReceta = filter_input(INPUT_POST, 'dificultad', FILTER_SANITIZE_NUMBER_INT);
	$tiempoReceta = filter_input(INPUT_POST, 'horas', FILTER_SANITIZE_NUMBER_INT) * 60 + filter_input(INPUT_POST, 'minutos', FILTER_SANITIZE_NUMBER_INT);
	$personasReceta = filter_input(INPUT_POST, 'personas', FILTER_SANITIZE_NUMBER_INT);
	$ingredientesReceta = filter_input(INPUT_POST, 'ingrediente', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
	$pasosReceta = filter_input(INPUT_POST, 'paso', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
	// Insertar la receta en el documento XML
	echo addRecipe($tituloReceta, $autorReceta, $dificultadReceta, $tiempoReceta, $personasReceta, $ingredientesReceta, $pasosReceta);
} else {
	// Mostrar la página de añadir una receta
?>
<!-- PÁGINA DE SUBIR RECETA -->
<div id="page_upload" style="display: none;">
	<?php
	if (!isset($_SESSION['username'])) {
		// Mensaje solicitando iniciar sesión
		echo '<div class="note"><p><a href="#page_login">Identif&iacute;cate</a> para acceder a todas las funciones de ChefMeUp!</p></div>' . "\r\n";
	} else {
		// Mostrar formulario para crear receta
	?>
	<form id="form_recipe" action="">
		<h1>Subir una receta</h1>
		<p>Título: <input type="text" name="titulo" /></p>
		<p>Dificultad: <span id="dificultad"></span><span id="dificultadText">Muy fácil</span></p>
		<p>Para <input name="personas" value="2" /> persona(s)</p>
		<p>Tiempo necesario: <input name="horas" value="0" /> hora(s) y <input name="minutos" value="15" /> minutos</p>
		<div id="ingredientes">
			<p>Ingrediente: <input type="text" name="ingrediente[]" /></p>
		</div>
		<p><input type="button" id="addIngredient" value="Añadir ingrediente" /></p>
		<div id="pasos">
			<p>Paso: <input type="text" name="paso[]" /></p>
		</div>
		<p><input type="button" id="addStep" value="Añadir paso" /></p>
		<p class="center"><input type="submit" value="Enviar receta" /></p>
		<p class="loading center"><img src="images/loading.gif" alt="Cargando..." width="220" height="19" /></p>
		<p class="message center"></p>
	</form>
	<?php
	}
	?>
</div>
<?php } ?>