<!-- PÁGINA DE MIS RECETAS -->
<div id="page_myrecipes" style="display: none;">
<?php 

// Utilizar las funciones de gestión de recetas
require_once dirname(__FILE__) . '/../recipes.inc.php';

// Iniciar/reanudar la sesión de PHP (si no se ha hecho ya)
if (session_id() === '') {
	session_start();
}

if (!isset($_SESSION['username'])) {
	// Mensaje solicitando iniciar sesión
	echo '<div class="note"><p><a href="#page_login">Identif&iacute;cate</a> para acceder a todas las funciones de ChefMeUp!</p></div>' . "\r\n";
} else {
	// Mostrar una lista con las recetas del usuario
	$recetas = simplexml_load_file('recetas.xml');
	$alguna = false;
	foreach ($recetas->receta as $receta) {
		if($_SESSION['username'] == $receta->autor) {
			$alguna = true;
			echo getRecipeHeader($receta['id'], true);		
		}
	}
	// Si el usuario no ha subido ninguna receta, invitarlo a hacerlo
	if ($alguna == false) {
		echo '<div class="note"><p>Parece que a&uacute;n no has subido ninguna receta.<br/>&iquest;Por qu&eacute; no <a href="#page_upload">a&ntilde;ades</a> una?</p></div>' . "\r\n";
	}
}

?>
</div>
