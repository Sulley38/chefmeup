<!-- PÁGINA INICIAL -->
<div id="page_start">
	<div id="start-intro">
		<h1>&iexcl;Bienvenid@ a ChefMeUp!</h1>
		<p>Tu cat&aacute;logo de recetas on-line donde podr&aacute;s descubrir y compartir las mejores recetas de cocina que se te ocurran.</p>
		<p>&iquest;Te gustar&iacute;a desenvolverte mejor en la cocina? &iquest;Necesitas una idea? &iexcl;Este es tu sitio!</p>
	</div>
	<?php
	if (isset($_SESSION['username'])) {
		// Mensaje que indica el usuario actual
		echo '<div class="note"><p>Has iniciado sesi&oacute;n como <span>' . $_SESSION['username'] . '</span></p></div>';
	} else {
		// Mensaje solicitando iniciar sesión
		echo '<div class="note"><p><a href="#page_login">Identif&iacute;cate</a> para acceder a todas las funciones de ChefMeUp!</p></div>' . "\r\n";
	}
	?>
	<div id="new-recipes" class="module">
		<h1 class="title">&Uacute;ltimas recetas a&ntilde;adidas:</h1>
		<?php
		// Utilizar las funciones de gestión de recetas
		require_once dirname(__FILE__) . '/../recipes.inc.php';
		
		// Cargar las recetas
		$recetasOrdered = simplexml_load_file('recetas.xml');
		$recetasOrdered = $recetasOrdered->xpath('/recetas/receta');
		
		// Ordenarlas por fecha
		$GLOBALS['cmpCriterio'] = 'fecha';
		$GLOBALS['cmpOrden'] = -1;
		usort($recetasOrdered, 'recipeIntCmp');
		
		// Imprimir las últimas 3 recetas
		echo getRecipeHeader($recetasOrdered[0]['id']);
		echo getRecipeHeader($recetasOrdered[1]['id']);
		echo getRecipeHeader($recetasOrdered[2]['id']);
		?>
	</div>
</div>
