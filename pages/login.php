<?php

// Utilizar las funciones de gestión de usuarios
require_once dirname(__FILE__) . '/../users.inc.php';

// Iniciar/reanudar la sesión de PHP (si no se ha hecho ya)
if (session_id() === '') {
	session_start();
}

// Si el usuario ya ha iniciado sesión, devolverlo a la página principal
if (isset($_SESSION['username'])) {
	header('Location: ../index.php');
	exit;
}

// Si recibimos parámetros por POST, procesarlos
if (isset($_POST['login']) && isset($_POST['username']) && isset($_POST['password'])) {
	
	/** Iniciar sesión **/
	// Sanear datos de entrada
	$username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
	$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
	// Buscar el par usuario/contraseña en la base de datos
	$result = checkLogin($username, $password);
	// Si es correcto
	if ($result === 'OK') {
		// Obtener un nuevo identificador de sesión, eliminando el anterior (si lo hubiera)
		session_regenerate_id(true);
		// Establecer la información de sesión
		$_SESSION['username'] = $username;
	}
	// Devolver el resultado (OK o WRONG)
	echo $result;
	
} else if (isset($_POST['register']) && isset($_POST['username']) && isset($_POST['password']) && isset($_POST['email']) && isset($_POST['city'])) {
	
	/** Crear una cuenta **/
	// Sanear datos de entrada
	$username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
	$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
	$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
	$city = filter_input(INPUT_POST, 'city', FILTER_SANITIZE_STRING);
	// Validar datos del formulario
	if ((strlen($username) >= 3) && (strlen($password) >= 6) && (preg_match('/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/', $email) === 1)) {
		// Introducir los datos del usuario en la base de datos y devolver el resultado (OK, USER_EXISTS o EMAIL_EXISTS)
		echo insertNewUser($username, $password, $email, $city);
	} else {
		// Error en los datos
		echo 'BAD_DATA';
	}
	
} else if(isset($_POST['recover']) && isset($_POST['email'])) {
	
	/** Constraseña olvidada **/
	// Sanear datos de entrada
	$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
	// Reestablecer la contraseña y devolver el resultado (OK, NOT_FOUND o NOT_EMAILED)
	echo resetPassword($email);
	
} else {
	/** Mostrar el formulario **/
?>
<!-- PÁGINA DE INICIAR SESIÓN -->
<div id="page_login" style="display: none;">
	<div id="login" class="module">
		<form id="form_login" action="">
			<p><input type="text" name="username" value="Nombre de usuario" /></p>
			<p><input type="text" name="password" value="Contraseña" /></p>
			<p><input type="hidden" name="login" value="1" /><input type="submit" value="Iniciar sesión" /></p>
			<p class="loading"><img src="images/loading.gif" alt="Cargando..." width="220" height="19" /></p>
			<p class="message"></p>
		</form>
		<hr/>
		<form id="form_recover" action="">
			<p><input type="text" name="email" value="Correo electrónico" /></p>
			<p><input type="hidden" name="recover" value="1" /><input type="submit" value="He olvidado mi contraseña" /></p>
			<p class="loading"><img src="images/loading.gif" alt="Cargando..." width="220" height="19" /></p>
			<p class="message"></p>
		</form>
	</div>
	<div id="register" class="module">
		<form id="form_register" action="">
			<h1>&iquest;Usuario nuevo?</h1>
			<h2>Reg&iacute;strese para disfrutar de todos los contenidos del sitio</h2>
			<p><input type="text" name="username" value="Nombre de usuario" /></p>
			<p><input type="text" name="password" value="Contraseña" /></p>
			<p><input type="text" name="password2" value="Repetir contraseña" /></p>
			<p><input type="text" name="email" value="Correo electrónico" /></p>
			<p><input type="text" name="email2" value="Repetir correo electrónico" /></p>
			<p><input type="text" name="city" value="Localidad" /></p>
			<p><input type="hidden" name="register" value="1" /><input type="submit" value="Crear una cuenta" /></p>
			<p class="loading"><img src="images/loading.gif" alt="Cargando..." width="220" height="19" /></p>
			<p class="message"></p>
		</form>
	</div>
</div>
<?php } ?>