<?php

// Iniciar/reanudar sesión de PHP
session_start();

// Destruir todas las variables de sesión.
$_SESSION = array();

// Borrar la cookie de sesión
$params = session_get_cookie_params();
setcookie(session_name(), '', time() - 42000, $params['path'], $params['domain'], $params['secure'], $params['httponly']);

// Destruir la sesión.
session_destroy();

// Redirigir a la página de inicio
header('Location: ../index.php');

?>