<?php

// Utilizar las funciones de gestión de recetas
require_once dirname(__FILE__) . '/../recipes.inc.php';

// Iniciar/reanudar la sesión de PHP (si no se ha hecho ya)
if (session_id() === '') {
	session_start();
}

// Valores posibles de los campos <select>
$valoresOrder = array('Ascendente', 'Descendente');
$valoresRule = array('Puntuación', 'Fecha', 'Dificultad', 'Tiempo', 'Autor', 'Título');
$valoresResults = array('5', '10', '15', '20', '30', '50');

// Si recibimos parámetros por POST, procesarlos
if(isset($_POST['id']) && isValidRecipeId($_POST['id'])) {
	
	/** Solicitud sobre una receta en concreto **/
	if (isset($_POST['vote']) && filter_input(INPUT_POST, 'vote', FILTER_VALIDATE_INT)) {
		// Solicitud de voto para la receta
		if ($_POST['vote'] == 1) {
			echo voteRecipe($_POST['id'], true); // Positivo
		} else if ($_POST['vote'] == -1) {
			echo voteRecipe($_POST['id'], false); // Negativo
		} else {
			echo '<p>Error inesperado</p>'; // Inválido
		}
	} else if (isset($_POST['remove'])) {
		// Solicitud de eliminación de la receta:
		// Comprobar que estamos logueados y que el usuario es el autor de la receta
		if (isset($_SESSION['username']) && strcmp($_SESSION['username'], getRecipeAuthor($_POST['id'])) == 0) {
			// Eliminar la receta
			echo removeRecipe($_POST['remove']);
		} else {
			echo '<p>Error inesperado</p>'; // Inválido
		}
	} else {
		// Solicitud del contenido de la receta
		echo getRecipeContent($_POST['id']);
	}
	
} else if (isset($_POST['rule']) && isset($_POST['order']) && isset($_POST['results'])) {
	
	/** Solicitud sobre la lista de recetas **/
	// Comprobar que los valores recibidos son válidos
	if (in_array($_POST['rule'], $valoresRule) && in_array($_POST['order'], $valoresOrder) && in_array($_POST['results'], $valoresResults)) {
		// Si nos piden una página en concreto, devolver esa; de lo contrario, devolver la primera
		if (isset($_POST['page']) && filter_input(INPUT_POST, 'page', FILTER_VALIDATE_INT) && $_POST['page'] > 0)
			echo getRecipeList($_POST['page'], $_POST['rule'], $_POST['order'], $_POST['results']);
		else
			echo getRecipeList(1, $_POST['rule'], $_POST['order'], $_POST['results']);
		
		// Añadir los parámetros de la petición para poder repetir la consulta por AJAX mediante el menú de navegación
		echo '<p id="list-params" class="'.$_POST['rule'].' '.$_POST['order'].' '.$_POST['results'].'" style="display: none;"></p>';
	}
	
} else {
	/** Mostrar el formulario **/
?>
<!-- PÁGINA DE LISTADO DE RECETAS -->
<div id="page_listrecipes" style="display: none;">
	<form id="form_listrecipes" action="">
		<h1>Explorador de recetas</h1>
		<table><tbody><tr>
			<td>
				Criterio: 
				<select name="rule">
					<?php foreach ($valoresRule as $valor) echo "<option>$valor</option>"; ?>
				</select>
			</td>
			<td>
				Orden: 
				<select name="order">
			  		<?php foreach ($valoresOrder as $valor) echo "<option>$valor</option>"; ?>
				</select>
			</td>
			<td>
				Resultados por p&aacute;gina: 
				<select name="results">
					<?php foreach ($valoresResults as $valor) echo "<option>$valor</option>"; ?>
				</select>
			</td>
			<td>
				<input type="submit" value="Mostrar" />
			</td>
		</tr></tbody></table>
	</form>
	<div id="recipelist"></div>
</div>
<?php
}
?>