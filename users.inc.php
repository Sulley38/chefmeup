<?php 

/**
 * Abrir la base de datos de usuarios.
 * Si el fichero no existe, se crea automáticamente.
 */
function openUsersDb() {
	try {
		$db = new PDO('sqlite:' . dirname(__FILE__) . '/users.db');
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $db;
	} catch (PDOException $e) {
		die('Error al conectar con la base de datos: ' . $e->getMessage);
	}
}

/**
 * Comprueba si existe el par usuario/contraseña en la base de datos.
 * Valores de retorno:
 * 		OK si el par existe
 * 		WRONG en caso contrario
 */
function checkLogin($username, $password) {
	// Abrir el fichero SQLite
	$db = openUsersDb();
	// Buscar un usuario con el nombre dado
	$sth = $db->prepare('SELECT password FROM Users WHERE username = ?');
	$sth->execute(array($username)) or  die('Error en la consulta:' . print_r($sth->errorInfo(), true));
	// Guardar la información del usuario encontrado
	$userdata = $sth->fetch();
	// Cerrar el fichero
	$sth = null;
	$db = null;
	// Devolver true si el usuario existe y la contraseña es correcta
	if ($userdata !== false && crypt($password, $userdata[0]) == $userdata[0])
		return 'OK';
	else
		return 'WRONG';
}

/**
 * Añade un nuevo usuario a la base de datos.
 * Valores de retorno:
 * 		OK si el usuario se ha añadido correctamente
 * 		USER_EXISTS si existe un usuario con el mismo nombre
 * 		EMAIL_EXISTS si existe una cuenta con ese e-mail registrado
 */
function insertNewUser($username, $password, $email, $city) {
	// Calcular el hash de la contraseña con una sal aleatoria
	$hash = crypt($password);
	// Abrir el fichero SQLite
	$db = openUsersDb();
	// Comprobar que el nombre de usuario no está cogido
	// Los '?' en una sentencia se sustituyen por parámetros del usuario al ejecutarla
	$sth = $db->prepare('SELECT * FROM Users WHERE username = ?') or die('Error en la consulta:' . print_r($sth->errorInfo(), true));
	$sth->execute(array($username));
	if (count($sth->fetchAll()) > 0)
		return 'USER_EXISTS';
	// Comprobar que el e-mail no está registrado
	$sth = $db->prepare('SELECT * FROM Users WHERE email = ?') or die('Error en la consulta:' . print_r($sth->errorInfo(), true));
	$sth->execute(array($email));
	if (count($sth->fetchAll()) > 0)
		return 'EMAIL_EXISTS';
	// Insertar el usuario en la base de datos
	$sth = $db->prepare('INSERT INTO Users (username, password, email, city) VALUES (?, ?, ?, ?)');
	$sth->execute(array($username, $hash, $email, $city)) or die('Error en la consulta:' . print_r($sth->errorInfo(), true));
	// Cerrar el fichero
	$sth = null;
	$db = null;
	// Devolver OK para indicar que la ejecución ha tenido éxito
	return 'OK';
}

/**
 * Establece una nueva contraseña para el usuario cuyo e-mail coincide con el parámetro.
 * Valores de retorno:
 * 		OK si el servidor de correo ha aceptado el e-mail con la nueva contraseña
 * 		NOT_FOUND si no existe un usuario con el e-mail indicado
 * 		NOT_EMAILED si el servidor de correo rechaza la petición de envío de e-mail
 */
function resetPassword($email) {
	// Generar una contraseña aleatoria de 8 caracteres
	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	$password = substr(str_shuffle($chars), 0, 8);
	// Calcular el hash de la contraseña con una sal aleatoria
	$hash = crypt($password);
	// Abrir el fichero SQLite
	$db = openUsersDb();
	// Preparar la sentencia SQL
	$sth = $db->prepare('UPDATE Users SET password = ? WHERE email = ?');
	// Ejecutar la sentencia con los valores del usuario
	$sth->execute(array($hash, $email)) or die('Error en la consulta:' . print_r($sth->errorInfo(), true));
	// Comprobar si ha tenido éxito: número de filas modificadas en la base de datos
	if ($sth->rowCount() != 1)
		return 'NOT_FOUND';
	// Cerrar el fichero
	$sth = null;
	$db = null;
	// Enviar email al usuario con la nueva contraseña
	$cabecera = 'From: info@chefmeup.com' . "\r\n";
	$cabecera .= 'MIME-Version: 1.0' . "\r\n";
	$cabecera .= 'Content-type: text/plain; charset=UTF-8' . "\r\n";
	if (mail($email, 'Nueva contraseña en ChefMeUp', 'Su nueva contraseña en ChefMeUp es: ' . $password . "\r\n", $cabecera))
		return 'OK';
	else
		return 'NOT_EMAILED';
}

?>