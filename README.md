# Chef Me Up! #

### By Iván Matellanes and Asier Mujika ###

Chef Me Up! is a website where you can share your best cooking recipes and learn new ideas and tips from other users. This project was developed on November 2013 to test our abilities to code using web technologies like HTML, PHP and AJAX.

We tried to combine different web technologies and take as much advantage as possible from them. The design was made with HTML and CSS, client-side programming in JavaScript (including AJAX and the popular libraries jQuery and jQuery UI), and server-side programming in PHP, relying on XML and SQLite for data persistence.


# Chef Me Up! #

### Por Iván Matellanes y Asier Mujika ###

Chef Me Up! es un sitio web en el que puedes compartir tus mejores recetas de cocina y aprender nuevas ideas y trucos de otros usuarios. Este proyecto fue desarrollado en noviembre de 2013 para probar nuestras habilidades en desarrollo web con tecnologías tipo HTML, PHP y AJAX.

Intentamos combinar diferentes tecnologías y aprovechar cada una de ellas lo máximo posible. El diseño fue realizado con HTML y CSS, la programación de la parte del cliente en JavaScript (incluyendo AJAX y las populares librerías jQuery y jQuery UI), y la programación del servidor en PHP, basándose en XML y SQLite para la persistencia de datos.
