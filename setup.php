<?php 

require 'users.inc.php';

// Abrir el fichero SQLite
$db = openUsersDb();

// Consulta para crear la tabla que contendrá información de usuarios
$query = 'CREATE TABLE Users (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			username TEXT NOT NULL UNIQUE,
			password TEXT NOT NULL,
			email TEXT NOT NULL UNIQUE,
			city TEXT
		  )';

// Ejecutar la consulta
if ($db->exec($query) === false)
	die(print_r($db->errorInfo(), true));

// Cerrar el fichero
$db = null;

// Introducir un par de usuarios a modo de prueba
insertNewUser('ivan', '1234', 'imatellanes003@ikasle.ehu.es', 'Pasaia');
insertNewUser('asier', 'sucker', 'amujika009@ikasle.ehu.es', 'Donostia');

echo '<p>Base de datos creada con &eacute;xito</p>';

?>